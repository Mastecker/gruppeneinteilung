# Gruppeneinteilung

* [Kurs als Ebook](https://mastecker.gitlab.io/gruppeneinteilung/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/gruppeneinteilung/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/gruppeneinteilung/index.html)
