# „Gruppeneinteilung“
## …mit abwechslungsreichen Methoden.
### Gruppenarbeit
Bei der Arbeit mit Lerngruppen kommt man um die Einteilung der großen Lerngruppe in kleinere Gruppen kaum drum herum. Für die Einteilung in Kleingruppen gibt es verschiedene Methoden, die angewendet werden können, um die Teilnehmenden zu aktivieren, das Lernklima zu verbessern, neue Konstellationen zu schaffen und das soziale Lernen der Teilnehmenden zu fördern.

### Möglichkeiten zur Gruppeneinteilung
Im Folgenden werden verschiedene Methoden vorgestellt, wie Gruppen (verschiedener Größen) gebildet werden können.
1.	Freiwillige Gruppenbildung
Für die Teilnehmer ist dies oftmals die angenehmste Art Gruppen zu bilden. Jedoch ist die Wahrscheinlichkeit sehr hoch, dass sich immer wieder die gleichen Teilnehmenden zu einer Gruppe zusammenfinden und das soziale Lernen in der gesamten Lerngruppe nicht gefördert wird. Werden jedoch persönliche Themen oder beispielsweise Sexualkunde unterrichtet, eignet sich diese Art der Gruppeneinteilung besonders, um niemanden in eine unan-genehme Situation zu bringen.

2.	Zufällige Gruppenbildung
Durch das zufällige Ziehen von Losen, Karten oder anderen Gegenständen werden immer wieder neue Gruppen gebildet und die Teilnehmer lernen, auch mal mit anderen zusammen zu arbeiten. So werden feste Gruppen aufgebrochen. Besonders bei jüngeren Teilnehmenden erhöht sich das Risiko, dass Störungen auftreten. Daher sollten sie es so früh wie möglich lernen, damit diese Art der Gruppeneinteilung als normal empfunden wird. Im Folgenden einige Anregungen und Inspirationen, wie die Gruppen zufällig gebildet werden können.

*Gruppeneinteilung mit Bildkarten*

Die Teilnehmer können aus unterschiedlichen Bildkarten auswählen, halten diese zunächst verdeckt. Wenn alle Teilnehmer eine Karte ausgewählt haben, werden sie aufgedeckt und die Teilnehmer mit denselben Karten finden sich zu Gruppen zusammen. Diese Methode kann abhängig von der Grup-pengröße individuell angepasst werden. Die Bilder können dem Thema oder Fachbereich entsprechend ausgewählt werden:
-	Tiere
-	Obstsorten
-	Würfelbilder
-	Zahlen oder Buchstaben

*Gruppeneinteilung mit Zitatekarten* 

Die Gruppenbildung mit Karten, auf denen Zitate/Sprüche/Redewendungen stehen, eignet sich besonders für ältere Teilnehmer und kann fachspezifisch eingesetzt werden. 
-	Im Fach Biologie können z.B. Definitionen (auf der einen Seite den Begriff, auf der an-deren die Definition) eingesetzt werden.

*Gruppeneinteilung mit Puzzle-Teilen*

Im Vorfeld wird vom Seminarleiter zugehörig zum Fach oder Themenbereich eine Postkarte / ein Zeitungsauschnitt / ein Bild zu einem Puzzle vorbereitet und jeder Teilnehmer zieht zufällig ein Puzzleteil. Dann müssen sich die Teilnehmer untereinander finden und die Puzzleteile zusammensetzen. Als Anreiz könnte die Gruppe, die sich am schnellsten gefunden hat, einen Vorteil für die anstehende Gruppenarbeit / eine kleine Belohnung erarbeiten.

*Gruppeneinteilung mit Überraschungseiern*

Der Seminarleiter präpariert Überraschungseier mit unterschiedlichen Gegenständen (Reis, Nüssen, Erbsen, Sand etc.). Jeder Teilnehmer erhält eine Dose und muss die dazugehörigen Mitschüler finden (durch Schütteln der Eier erklingt ein anderer Ton).

*Gruppeneinteilung mit Familie Meyer*

Alternative I: Der Seminarleiter hat Karten mit unterschiedlichen Familiennamen, die ähnlich klingen, vorbereitet. Die Teilnehmer müssen genau hören, welcher Teilnehmer den gleichen Familiennamen trägt.
- z.B. Vater Meyer, Mutter Meyer, Tochter Meyer, Sohn Meyer, Hund Meyer // Vater Leier, Mutter Leier, Tochter Leier, Sohn Leier, Hund Leier // Weiter mit Familiennamen: Geier, Seier, Beyer, Heyer, etc.

Alternative II: Es werden verschiedene Schreibweisen für Familie Meyer/Meier/Maier/Mayer/Meia/usw. verwendet. 

*Gruppeneinteilung mit verschiedenen Gegenständen*

In einem Vorrat befinden sich verschiedene Gegenstände aus denen jede/r Teilnehmende sich einen aussuchen kann. Daraufhin werden die Teilnehmenden untereinander ihre Gegenstände zeigen und sich zu Gruppen zusammenfinden. Die Gegenstände können beliebig erweitert oder verändert und der Gruppengrößte angepasst werden.
Beispiel: 
- Gabel, Messer, Löffel
- Faden, Nadel, Socke, Knöpfe
- 1 Cent, 2 Cent, 5 Cent, 10 Cent, 20 Cent
- Bleistift, Radiergummi, Geodreieck
- Tuschkasten, Wachsmalstifte, Pinsel

*Gruppeneinteilung mit Losen*

Vorbereitete Lose mit unterschiedlichen Symbolen oder Farben werden gezogen und bilden die Gruppen.

*Gruppeneinteilung mit Kartenspiel*

Man nimmt ein einfaches Kartenspiel, jede/r Teilnehmende zieht eine Karte, dann gibt es je nach gewünschter Gruppengröße verschiedene Möglichkeiten:
-	rote / schwarze Karten
-	Karo / Pik / Herze / Kreuze
-	Siebener / Achter / Neuner / etc.
Die Karten müssen vorher natürlich der Gesamtgruppengröße angepasst werden.

*Gruppeneinteilung durch Durchzählen*

Eine der wohl bekanntesten Methoden ist es, den Teilnehmern eine Zahl durch Durchzählen zuzuordnen (z.B. 1, 2, 3 – 1, 2, 3 – 1, 2, 3 usw.). Dies ist eine Methode, die weder Vorbereitung noch Material benötigt und spontan eingesetzt werden kann.

*Besuch im Zoo*

Diese Methode eignet sich besonders für jüngere Schüler. Jede/r Teilnehmende erhält eine Karte auf der ein Tiername steht, dieser wird noch geheim gehalten. Solange Musik gespielt wird, laufen alle durch den Raum, wird die Musik gestoppt, macht jeder sein Tiergeräusch und sucht „Gleichgesinnte“.

*Marktplatz*

Jeder Teilnehmer erhält einen Gegenstand (abhängig von der Gruppengrößte insgesamt z.B. drei Kugelschreiber, drei Marker, drei Geodreiecke, drei Radiergummis etc.), solange Musik läuft, darf getauscht werden. Wenn die Musik ausgeschaltet ist, finden sich die Gruppen zusammen.

*Gruppeneinteilung mit Süßigkeiten*

Für die Einteilung von beliebig großen Gruppen eignen sich auch Süßigkeiten, die besonders durch ihre unterschiedlichen Farben dazu geeignet sind. Beispiele dafür sind: Nimm2-Bonbons, Gummibärchen, Smarties, Mini-Snickers/Twix/Milky-Way/Mars uvm.

*Gruppeneinteilung nach Kategorien*

Die Teilnehmenden finden sich nach Geburtsmonat, Sternzeichen, Schuhgröße, Körpergröße, Wohnort o.ä. zusammen.

*Gruppeneinteilung vom Seminarleiter festgelegt*

Der Seminarleiter markiert vorher jeden Stuhl der Teilnehmenden mit einem farbigen Klebepunkt, nach denen sich dann die Gruppen finden können. Wenn ein Sitzplan bekannt ist, kann die Zusammensetzung der Gruppen geplant werden. Finden sich die Sitzplätze zufällig, erfolgt auch die Gruppeneinteilung zufällig.

III.	Onlinewerkzeug als Gruppengenerator
Im Internet gibt es verschiedene Seiten, auf denen die Gruppe in Kleingruppe zufällig eingeteilt werden kann.
Beispiele:
- 	LearningApps
Unter dem Reiter „Unterrichtswerkzeuge“ findet sich das Werkzeug „Gruppen bilden“. Auf dem Schild „ähnliche App bilden“ können die Namen der eigenen Gruppenmitglieder eingegeben werden. Dann kann immer wieder aufs Neue ausgewählt werden, ob die Gesamtgruppe in 2er, 3er, 4er, 5er, 6er oder 7er Gruppen eingeteilt oder die Gruppe durch zwei, drei, vier etc. geteilt werden soll. Ebenfalls berücksichtigt werden kann die Aufteilung von Jungen und Mädchen.
Ein weiterer Zusatz ist, dass der Generator auch auswählen kann, wer als nächster an der Reihe ist.
- Ultimatesolver.com
Eine weitere Website auf der die Namen der Teilnehmer eingegeben werden und in nullkommanix in beliebig große Gruppen eingeteilt werden können. 
- Viele SmartBoards haben schon einen Zufallsgenerator im Softwarerepertoir. Fragen Sie in dem E-Learning Service nach Sup-port.


#### Quellen
- http://www.super-sozi.de/index.php/spielekartei/actionspiele-8
- https://www.hueber.de/media/36/Gruppen-bilden.pdf

